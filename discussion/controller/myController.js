const Task = require("../models/User.js");

module.exports.getTask = (paramsId) => {

	return Task.findById(paramsId).then((result, err) => {
		if(result !== null){
			return result
		} else {
			return "Something went Wrong!"
		}
	}) 
};

module.exports.createTasks = (requestBody) => {
	return Task.findOne({name: requestBody.name}).then((result, error) => {
		if(result !== null && result.name == requestBody.name){
			return 'Duplicate Task Found.'
		} else {
			let newTask = new Task({
				firstName : requestBody.firstName,
				lastName : requestBody.lastName,
				email : requestBody.email,
				password : requestBody.password,
				isAdmin : requestBody.isAdmin,
				mobileNo : requestBody.mobileNo,
				enrollments : requestBody.enrollments
			})

			return newTask.save(requestBody).then((savedTask, error) => {
				if(savedErr) {
					console.log(savedTask)
					return 'Task Creation Failed';
				} else {
					return savedTask;
				}
			})
		}
	})
};


module.exports.updateTask = (paramsId) => {
	return Task.findByIdAndUpdate(paramsId, {status : "complete"}).then((result, err) => {
		if (result.status == "complete"){
			return "The Task Status was set to \"Complete\" Already!";
		} else {
			return result, "Status changed to \"Complete\"";
		}
	})
}