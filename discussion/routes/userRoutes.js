const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js")

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for user authentication
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

})

/* router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
}) */

// Activity s38

router.post("/details", /*auth.verify, */(req, res) => {

	// const userData = auth.decode(req.headers.authorization);

	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;